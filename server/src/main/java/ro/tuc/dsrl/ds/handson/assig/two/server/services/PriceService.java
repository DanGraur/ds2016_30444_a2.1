package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

/**
 * Created by noi on 11/10/2016.
 */
public class PriceService implements IPriceService {

    @Override
    public double computePrice(Car c) {

        if (c.getPrice() <= 0.0)
                throw new IllegalArgumentException("The price of the car cannot be less than or equal to 0!");

        return c.getPrice() - (c.getPrice() / 7.0) * (2016 - c.getYear());
    }

}
