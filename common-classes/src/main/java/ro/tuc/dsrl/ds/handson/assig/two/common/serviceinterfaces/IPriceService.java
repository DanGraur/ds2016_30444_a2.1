package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by noi on 11/10/2016.
 */
public interface IPriceService {

    /**
     * Computes the selling price of the car sent as a parameter
     *
     * @param c a Car object whose selling price will be calculated
     */
    double computePrice(Car c);

}
